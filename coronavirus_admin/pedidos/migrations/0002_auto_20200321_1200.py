# Generated by Django 2.2.11 on 2020-03-21 15:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pedidos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='endereco',
            name='complemento',
            field=models.CharField(blank=True, help_text='Por exemplo, apto 402', max_length=50, null=True),
        ),
    ]
